from django.urls import path, include
from rest_framework import routers

from sales.viewset import SalesViewSet, ArticleViewSet


router = routers.DefaultRouter(trailing_slash=False)
router.register(r'sales', SalesViewSet)
router.register(r'articles', ArticleViewSet, basename='article')

urlpatterns = [
    path(
        "v1/",
        include(
            [
                path('auth/', include('dj_rest_auth.urls')),
                path("", include(router.urls)),
            ]
        ),
    )
]
