from rest_framework import serializers

from sales.models import Sale, Article

class ArticleSerializer(serializers.ModelSerializer):
    category = serializers.StringRelatedField()
    class Meta:
        model = Article
        fields = ['code', 'category', 'name', ]

class SaleSerializer(serializers.ModelSerializer):
    article = ArticleSerializer(read_only=True)
    class Meta:
        model = Sale
        fields = ['id', 'date', 'author', 'article', 'quantity', 'unit_selling_price', 'total_selling_price', 'article', ]