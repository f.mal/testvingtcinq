from rest_framework import viewsets, permissions
from rest_framework.response import Response
from django.forms.models import model_to_dict

from sales.models import Sale, Article
from sales.serializers import SaleSerializer



## Permission

class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Permission to modify only if the object has the current user as owner
    Need a property author with user.User
    """
    message = 'Only the sell owner has permission to make changes.'

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.author == request.user

## Viewsets

class SalesViewSet(viewsets.ModelViewSet):
    queryset = Sale.objects.all().select_related('article', 'article__category')
    serializer_class = SaleSerializer
    permission_classes = [permissions.IsAuthenticated, IsOwnerOrReadOnly]


class ArticleViewSet(viewsets.ViewSet):
    
    def list(self, request):
        queryset = Article.objects.all().select_related('category').prefetch_related('sales')
        
        results_as_objects = [article for article in queryset]
        results_as_objects.sort(
            key=lambda x: x.get_total_amount_sales(), reverse=True
        )

        results_as_dict = []
        for article in results_as_objects:
            article_as_dict = model_to_dict(article)
            last_sale = article.get_last_sale()
            article_as_dict.update({
                'category_name' : str(article.category), 
                'total_amount_sales' : article.get_total_amount_sales(),
                'profit_percentage' : article.get_profit_percentage(),
                'last_sale' : model_to_dict(last_sale) if last_sale else None,
            })
            results_as_dict.append(article_as_dict)

        return Response(results_as_dict)

