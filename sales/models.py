from functools import partial

from django.db import models

PriceField = partial(models.DecimalField, max_digits=11, decimal_places=2)
DefaultCharField = partial(models.CharField, max_length=255)


class ArticleCategory(models.Model):
    """
    Category of an article
    """

    class Meta:
        verbose_name = "Article Category"
        verbose_name_plural = "Article Categories"

    objects = models.Manager()

    display_name = DefaultCharField("Display name", unique=True)

    def __str__(self):
        return f"{self.display_name}"


class Article(models.Model):
    """
    An article is an item that can be sold.
    """

    class Meta:
        verbose_name = "Article"
        verbose_name_plural = "Articles"

    objects = models.Manager()

    code = models.CharField("Code", max_length=6, unique=True)
    category = models.ForeignKey(
        ArticleCategory,
        verbose_name="Category",
        related_name="articles",
        on_delete=models.PROTECT,
    )
    name = DefaultCharField("Name")
    manufacturing_cost = PriceField("Manufacturing Cost")

    def __str__(self):
        return f"{self.code} - {self.name}"

    def get_total_amount_sales(self):
        amount = 0
        for sale in self.sales.all():
            amount += sale.total_selling_price
        return amount

    def get_profit_percentage(self):
        amount_sales = 0
        amount_costs = 0
        for sale in self.sales.all():
            amount_sales += sale.total_selling_price
            amount_costs += sale.quantity * self.manufacturing_cost
        return round(100*(amount_sales-amount_costs)/amount_sales, 2)

    def get_last_sale(self):
        last_sale = None
        for sale in self.sales.all():
            if sale.date:
                if last_sale is None:
                    last_sale = sale
                elif last_sale.date < sale.date:
                    last_sale = sale
        return last_sale


class Sale(models.Model):
    """
    A sale of an article.
    """

    class Meta:
        verbose_name = "Sale"
        verbose_name_plural = "Sales"

    objects = models.Manager()

    date = models.DateField("Date")
    author = models.ForeignKey(
        "users.User",
        verbose_name="Author",
        related_name="sales",
        on_delete=models.PROTECT,
    )
    article = models.ForeignKey(
        Article, verbose_name="Article", related_name="sales", on_delete=models.CASCADE
    )
    quantity = models.IntegerField("Quantity")
    unit_selling_price = PriceField("Unit selling price")

    def __str__(self):
        return f"{self.date} - {self.quantity} {self.article.name}"

    @property
    def total_selling_price(self):
        if self.unit_selling_price is None:
            return None
        return self.unit_selling_price * self.quantity
